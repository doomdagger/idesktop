To Do
=====

> Make a todo list, just for me.

We got plenty of things to do for iDesktop project. iDesktop is totally based on Ext JS. Apart from a lot of
improvements, we still have to make plenty of modifications on the original framework.

 To Do List:

> Finished:
* Modify the css for the shortcut

> Pending:
* Modify the position for the shortcut
* Add right click event for shortcut
* Add left bar for the desktop & make it pretty
* Add top bar for the desktop & make it pretty
* make the shortcut draggable
* make the shortcut have context menu
* Add more functions to the task bar, including enhancing the tray clock and add more icons for easy access
* Add Calendar module for the desktop
* Add Instant message module for the desktop